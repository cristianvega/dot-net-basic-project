﻿using AutoMapper;
using Pacagroup.Ecommerce.Application.DTO;
using Pacagroup.Ecommerce.Application.Interface;
using Pacagroup.Ecommerce.Domain.Entity;
using Pacagroup.Ecommerce.Domain.Interface;
using Pacagroup.Ecommerce.Transversal.Common;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Pacagroup.Ecommerce.Application.Main
{
    public class CustomerApplication: ICustomerApplication
    {
        private readonly ICustomersDomain _customerDomain;
        private readonly IMapper _mapper;

        public CustomerApplication(ICustomersDomain customerDomain, IMapper mapper)
        {
            _customerDomain = customerDomain;
            _mapper = mapper;
        }

        #region Metodos Sincronos
        public Response<bool> Insert(CustomerDTO customersDto)
        {
            var response = new Response<bool>();
            try
            {
                var customer = _mapper.Map<Customers>(customersDto);
                response.Data = _customerDomain.Insert(customer);
                if (response.Data)
                {
                    response.IsSuccess = true;
                    response.Message = "Registro Exitoso!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        public Response<bool> Update(CustomerDTO customersDto)
        {
            var response = new Response<bool>();
            try
            {
                var customer = _mapper.Map<Customers>(customersDto);
                response.Data = _customerDomain.Update(customer);
                if (response.Data)
                {
                    response.IsSuccess = true;
                    response.Message = "Actualizacion Exitoso!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        public Response<bool> Delete(string customerId)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = _customerDomain.Delete(customerId);
                if (response.Data)
                {
                    response.IsSuccess = true;
                    response.Message = "Eliminacion Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }

        public Response<CustomerDTO> Get(string customerId)
        {
            var response = new Response<CustomerDTO>();
            try
            {
                var customer = _customerDomain.Get(customerId);
                response.Data = _mapper.Map<CustomerDTO>(customer);
                if (response.Data!= null)
                {
                    response.IsSuccess = true;
                    response.Message = "Consulta Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        public Response<IEnumerable<CustomerDTO>> GetAll()
        {
            var response = new Response<IEnumerable<CustomerDTO>>();
            try
            {
                var customers = _customerDomain.GetAll();
                response.Data = _mapper.Map<IEnumerable<CustomerDTO>>(customers);
                if (response.Data != null)
                {
                    response.IsSuccess = true;
                    response.Message = "Consulta de todos Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        #endregion
        #region Metodos Asincronos
        public async Task<Response<bool>> InsertAsync(CustomerDTO customersDto)
        {
            {
                var response = new Response<bool>();
                try
                {
                    var customer = _mapper.Map<Customers>(customersDto);
                    response.Data = await _customerDomain.InsertAsync(customer);
                    if (response.Data)
                    {
                        response.IsSuccess = true;
                        response.Message = "Registro Exitoso!!";
                    }
                }
                catch (Exception ex)
                {
                    response.Message += ex.ToString();
                }
                return response;
            }
        }
        public async Task<Response<bool>> UpdateAsync(CustomerDTO customersDto)
        {
            var response = new Response<bool>();
            try
            {
                var customer = _mapper.Map<Customers>(customersDto);
                response.Data = await _customerDomain.UpdateAsync(customer);
                if (response.Data)
                {
                    response.IsSuccess = true;
                    response.Message = "Actualizacion Exitoso!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        public async Task<Response<bool>> DeleteAsync(string customerId)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = await _customerDomain.DeleteAsync(customerId);
                if (response.Data)
                {
                    response.IsSuccess = true;
                    response.Message = "Eliminacion Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }

        public async Task<Response<CustomerDTO>> GetAsync(string customerId)
        {
            var response = new Response<CustomerDTO>();
            try
            {
                var customer = await _customerDomain.GetAsync(customerId);
                response.Data = _mapper.Map<CustomerDTO>(customer);
                if (response.Data != null)
                {
                    response.IsSuccess = true;
                    response.Message = "Consulta Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        public async Task<Response<IEnumerable<CustomerDTO>>> GetAllAsync()
        {
            var response = new Response<IEnumerable<CustomerDTO>>();
            try
            {
                var customers = await _customerDomain.GetAllAsync();
                response.Data = _mapper.Map<IEnumerable<CustomerDTO>>(customers);
                if (response.Data != null)
                {
                    response.IsSuccess = true;
                    response.Message = "Consulta de todos Exitosa!!";
                }
            }
            catch (Exception ex)
            {
                response.Message += ex.ToString();
            }
            return response;
        }
        #endregion
    }
}
