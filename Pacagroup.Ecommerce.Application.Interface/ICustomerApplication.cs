﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pacagroup.Ecommerce.Application.DTO;
using Pacagroup.Ecommerce.Transversal.Common;

namespace Pacagroup.Ecommerce.Application.Interface
{
    public interface ICustomerApplication
    {
        #region Metodos Sincronos
        Response<bool> Insert(CustomerDTO customersDto);
        Response<bool> Update(CustomerDTO customersDto);
        Response<bool> Delete(string customerId);

        Response<CustomerDTO> Get(string customerId);
        Response<IEnumerable<CustomerDTO>> GetAll();
        #endregion
        #region Metodos Asincronos
        Task<Response<bool>> InsertAsync(CustomerDTO customers);
        Task<Response<bool>> UpdateAsync(CustomerDTO customers);
        Task<Response<bool>> DeleteAsync(string customerId);

        Task<Response<CustomerDTO>> GetAsync(string customerId);
        Task<Response<IEnumerable<CustomerDTO>>> GetAllAsync();
        #endregion
    }
}
