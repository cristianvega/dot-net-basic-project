﻿using AutoMapper;
using Pacagroup.Ecommerce.Domain.Entity;
using Pacagroup.Ecommerce.Application.DTO;

namespace Pacagroup.Ecommerce.Transversal.Mapper
{
    public class MappingsProfile: Profile
    {
        public MappingsProfile() { 
            CreateMap<Customers,CustomerDTO>().ReverseMap();
            CreateMap<Users, UsersDTO>().ReverseMap();
            //CreateMap<Customers, CustomerDTO>().ReverseMap()
            //    .ForMember(destination => destination.CustomerId, source => source.MapFrom(src => src.CustomerId);
        }
    }
}
