﻿namespace Pacagroup.Ecommerce.Application.DTO
{
    public class UsersDTO
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string lastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
